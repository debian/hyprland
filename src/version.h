#pragma once
#define GIT_COMMIT_HASH    "1c460e98f870676b15871fe4e5bfeb1a32a3d6d8"
#define GIT_BRANCH         "HEAD"
#define GIT_COMMIT_MESSAGE "    props: bump ver to 0.36.0"
#define GIT_COMMIT_DATE    "Wed Feb 28 00:32:40 2024"
#define GIT_DIRTY          ""
#define GIT_TAG            "v0.36.0"
